﻿using Common;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace E_NewsLetters {
    public partial class Form1 : Form {
        //https://docs.google.com/spreadsheets/d/1SSkGiah7AnpJM73uhflfuQ94_owwS7RvcY6TwcSGci0/edit?usp=sharing

        public Form1() {
            InitializeComponent();
            button3.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e) {
            try {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    Global.configPath = openFileDialog1.FileName;
                    List<String> list = Global.ReadFromExcel(Global.configPath, "config", 1);
                    Global.mailInfo.UserName = list[0];
                    Global.mailInfo.Password = list[1];
                    Global.mailInfo.MailServer = list[2];
                    Global.mailInfo.Port = int.Parse(list[3]);
                    Global.mailInfo.EnableSSL = bool.Parse(list[4]);
                    Global.subjects = list[5];
                    if (Global.configPath != String.Empty) {
                        button3.Enabled = true;
                    }
                    button1.Text = "Config Loaded";
                }
            } catch (Exception) {
                MessageBox.Show("Config File is invalid format","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            try {
                DoSendMail frm = new DoSendMail();
                frm.ShowDialog();
            } catch (Exception ex) {
                MessageBox.Show("Error "  + ex.Message);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            
        }
    }
}

