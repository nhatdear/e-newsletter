﻿using Common;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_NewsLetters {
    public class Global {
        public static String configPath = "";
        public static String templatePath = "";
        public static String subjects = "";
        public static MailServerInfo mailInfo = new MailServerInfo();
        public static List<String> ReadFromExcel(string path, string sheetName, int startRow) {
            NPOI.XSSF.UserModel.XSSFWorkbook hssfwb;
            List<String> list = new List<String>();
            using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read)) {
                hssfwb = new NPOI.XSSF.UserModel.XSSFWorkbook(file);
            }

            ISheet sheet = hssfwb.GetSheet(sheetName);
            for (int row = startRow; row <= sheet.LastRowNum; row++) {
                IRow currentRow = sheet.GetRow(row);
                if (currentRow != null) //null is when the row only contains empty cells 
                {
                    list.Add(currentRow.GetCell(1).ToString());
                }
            }

            return list;
        }
    }
}
