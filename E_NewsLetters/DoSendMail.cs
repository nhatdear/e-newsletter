﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace E_NewsLetters {
    public partial class DoSendMail : Form {
        List<String> emailList_en = new List<string>();
        List<String> emailList_vn = new List<string>();
        public DoSendMail() {
            InitializeComponent();
            // set initial image
            this.pictureBox.Image = Properties.Resources.InformationImage;
            emailList_en = Global.ReadFromExcel(Global.configPath, "emails_en", 1);
            emailList_vn = Global.ReadFromExcel(Global.configPath, "emails_vn", 1);
        }

        private void OnStartClick(object sender, EventArgs e) {
            // show animated image
            this.pictureBox.Image = Properties.Resources.AnimatedImage;
            // change button states
            this.buttonStart.Enabled = false;
            this.buttonCancel.Enabled = true;
            // start background operation
            this.backgroundWorker.RunWorkerAsync();
        }

        private void OnCancelClick(object sender, EventArgs e) {
            this.backgroundWorker.CancelAsync();
        }

        private void OnDoWork(object sender, DoWorkEventArgs e) {
            
            foreach (String email in emailList_en) {
                if (this.backgroundWorker.CancellationPending) {
                    e.Cancel = true;
                    break;
                }
                // report progress
                this.backgroundWorker.ReportProgress(-1, string.Format("Performing send mail to {0}...", email));
                // simulate operation step
                if (email.Trim() != String.Empty) {
                    String rls = EmailUtils.SendSimpleMailByContentString(Global.mailInfo, email, Global.subjects, Properties.Resources.MailTemplate_EN);

                    if (rls != "SUCCESS") {
                        MessageBox.Show("An error occurs when send email to " + email);
                    }
                }
            }

            foreach (String email in emailList_vn) {
                if (this.backgroundWorker.CancellationPending) {
                    e.Cancel = true;
                    break;
                }
                // report progress
                this.backgroundWorker.ReportProgress(-1, string.Format("Performing send mail to {0}...", email));
                // simulate operation step
                if (email.Trim() != String.Empty) {
                    String rls = EmailUtils.SendSimpleMailByContentString(Global.mailInfo, email, Global.subjects, Properties.Resources.MailTemplate_EN);

                    if (rls != "SUCCESS") {
                        MessageBox.Show("An error occurs when send email to " + email);
                    }
                }
            }
        }

        private void OnProgressChanged(object sender, ProgressChangedEventArgs e) {
            if (e.UserState is String) {
                this.labelProgress.Text = (String)e.UserState;
            }
        }

        private void OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // hide animation
            this.pictureBox.Image = null;
            // show result indication
            if (e.Cancelled) {
                this.labelProgress.Text = "Operation cancelled by the user!";
                this.pictureBox.Image = Properties.Resources.WarningImage;
            } else {
                if (e.Error != null) {
                    this.labelProgress.Text = "Operation failed: " + e.Error.Message;
                    this.pictureBox.Image = Properties.Resources.ErrorImage;
                } else {
                    this.labelProgress.Text = "Operation finished successfuly!";
                    this.pictureBox.Image = Properties.Resources.InformationImage;
                }
            }
            // restore button states
            this.buttonStart.Enabled = true;
            this.buttonCancel.Enabled = false;
        }

        private void DoSendMail_FormClosing(object sender, FormClosingEventArgs e) {
            this.backgroundWorker.CancelAsync();
        }
    }
}
