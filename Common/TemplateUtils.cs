﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Common
{
    public class TemplateUtils
    {
        public class myTemplate<T>
        {
            public static List<T> RandomList(List<T> myList)
            {
                Random rd = new Random();
                List<T> tmp = new List<T>();
                while (myList.Count > 0)
                {
                    int index = rd.Next(0, myList.Count - 1);
                    T temp = myList[index];
                    myList.Remove(temp);
                    tmp.Add(temp);
                }
                return tmp;
            }
            public static void CreateNewFromOldOne(ref T newOne, T oldOne)
            {
                for (int i = 0; i < oldOne.GetType().GetProperties().Count(); i++)
                {
                    try
                    {
                        string propertyName = oldOne.GetType().GetProperties()[0].Name;
                        PropertyInfo propertyInfoOldOne = oldOne.GetType().GetProperty(propertyName);
                        var newvalue = oldOne.GetType().GetProperty(propertyName).GetValue(oldOne, null);

                        PropertyInfo propertyInfoNewOne = newOne.GetType().GetProperty(propertyName);
                        propertyInfoNewOne.SetValue(newOne, Convert.ChangeType(newvalue, propertyInfoNewOne.PropertyType), null);
                    }
                    catch
                    {

                    }
                }
            }
        }
        public class myTemplate<T1, T2>
        {
            public static void CreateNewFromOldOne(ref T1 newOne, T2 oldOne)
            {
                for (int i = 0; i < oldOne.GetType().GetProperties().Count(); i++)
                {
                    try
                    {
                        string propertyName = oldOne.GetType().GetProperties()[0].Name;
                        PropertyInfo propertyInfoOldOne = oldOne.GetType().GetProperty(propertyName);
                        var newvalue = oldOne.GetType().GetProperty(propertyName).GetValue(oldOne, null);

                        PropertyInfo propertyInfoNewOne = newOne.GetType().GetProperty(propertyName);
                        propertyInfoNewOne.SetValue(newOne, Convert.ChangeType(newvalue, propertyInfoNewOne.PropertyType), null);
                    }
                    catch
                    {

                    }
                }
            }
        }
    }
}
