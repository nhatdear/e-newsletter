﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class UserInfo
    {
        private int _UserID;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        public string UserCode { get; set; }
        private string _UserName = String.Empty;
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string _Name = String.Empty;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public UserInfo() { }

        public UserInfo(int ID)
        {
            _UserID = ID;
        }
    }

    /// <summary>
    /// LoginInfo class
    /// </summary>
    /// <author>Nguyen Minh Tien</author>
    public class LoginInfo
    {
        public int   UserID { get; set; }
        public String UserName { get; set; }
        public string Password { get; set; }
        public ERROR_CODE.ERROR_CODE_LOGIN LoginResult { get; set; }
        public UserInfo UserInfo { get; set; }
        public LoginInfo() { }
        public LoginInfo(String username, String password)
        {
            UserName = username;
            Password = password;
        }
    }
}
