﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Mail_
    {
        private string to;
        private string subject;
        private string body;
        public Mail_(string to_, string subject_, string body_)
        {
            to = to_;
            subject = subject_;
            body = body_;
        }
        public void SendMail()
        {
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.To.Add(to);
                message.Subject = subject;
                message.From = new System.Net.Mail.MailAddress(Global.MailServerInfo_.UserName);
                message.Body = body;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(Global.MailServerInfo_.MailServer, Global.MailServerInfo_.Port);
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.Credentials = new System.Net.NetworkCredential(Global.MailServerInfo_.UserName, Global.MailServerInfo_.Password);
                smtp.EnableSsl = Global.MailServerInfo_.EnableSSL;
                smtp.Send(message);
               
            }
            catch(Exception ex)
            {
                string a = ex.Message;                
            }
        }
    }
}
