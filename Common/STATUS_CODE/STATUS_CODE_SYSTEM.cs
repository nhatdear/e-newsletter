﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace Common.STATUS_CODE
{
    public class IMPORT_CODE
    {
        public const string SUCCESS = "Success";
        public const string UPDATE = "Update";
        public const string SKIP = "Skip";
    }

    public class HISTORY_ACTION_CODE
    {
        public const string INSERT = "Insert";
        public const string UPDATE = "Update";
        public const string ORGINAL = "Orginal";
    }

    public class ROLE_CODE
    {
        public const string ROOT = "Root";
        public const string ADMIN = "Admin";
        public const string PGLeader = "PGLeader";
        public const string PG = "PG";
    }

    public enum ROW_STATUS
    {
        [DescriptionAttribute("Active.")]
        ACTIVE = 1,
        [DescriptionAttribute("Deactive.")]
        DEACTIVE = 2,
        [DescriptionAttribute("CreateNew.")]
        CREATENEW = 3,
        [DescriptionAttribute("Updated.")]
        UPDATED = 4,
        [DescriptionAttribute("Warn.")]
        WARN = 5,
        [DescriptionAttribute("Trùng tin nhắn.")]
        ERROR = 6,
        [DescriptionAttribute("SentSALE.")]
        SENTSALE = 7,
        [DescriptionAttribute("Finish.")]
        FINISH = 9,
        [DescriptionAttribute("Sai tin nhắn.")]
        ERROR2 = 10,
    }

    /// <summary> Phân biệt User WEB vs FORM </summary>
    public enum USER_ROW_STATUS
    {
        [DescriptionAttribute("Form Login")]
        FORM_LOGIN = 3,
        [DescriptionAttribute("Web Login")]
        WEB_LOGIN = 4,
    }

    /// <summary> Trạng thái của ContractorDetail_show </summary>
    public enum CONTRACTOR_DETAIL_SHOW
    {
        [DescriptionAttribute("Bình thường")]
        ACTIVE = 1,
        [DescriptionAttribute("Nghi ngờ hệ thống")]
        WARN_BY_SYS = 2,
        [DescriptionAttribute("Cần kiểm tra*")]
        NEED_CHECK = 3,
        [DescriptionAttribute("Lỗi sai tin nhắn*")]
        MESSAGE_ERROR = 4,
        [DescriptionAttribute("Lỗi trùng tin nhắn*")]
        MESSAGE_DUPLICATE = 5,
    }

    public enum SURVEY_STATUS
    {
        [DescriptionAttribute("Start.")]
        START_SURVEY = 1,
        [DescriptionAttribute("End.")]
        END_SURVEY = 2
    }
}
