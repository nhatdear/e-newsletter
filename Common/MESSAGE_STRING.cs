﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class MESSAGE_STRING
    {
        //public const string EMPTY_DATA = "Không có dữ liệu.";
        //public const string NULLTEXT_ERROR = "Vui lòng không để trống thông tin.";
        //public const string CONFIRM_DELETE = "Bạn có chắc xóa thông tin đã chọn không?";
        //public const string UPLOAD_FAIL = "Lỗi Upload.";
        //public const string IN_PROGRESS = "Đang xử lý. Vui lòng đợi.";
        public const string LOGIN_FAIL = "Sai tên đăng nhập.";


        /// <summary> Xác nhận: rỗng, đã tồn tại </summary>
        public class Validate
        {
            public const string NOT_NULL = "Không để trống giá trị này";
            public const string IS_EXIST = "Giá trị đã tồn tại";
            public const string ONLY_NUM = "Chỉ nhập số";
            
        }

        public class MessageShow
        {
            public static string NotNull(string _name)
            {
                return "Không để trống '" + _name + "'";
            }
            public static string IsExist(string _name)
            {
                return "'" + _name + "' đã tồn tại";
            }
        }

        public class AddNew
        {
            public static string Success(string _name)
            {
                return "Tạo mới '" + _name + "' thành công";
            }
            public static string Fail(string _name, string _error)
            {
                return "Tạo mới '" + _name + "' thất bại. Lỗi: " + _error;
            }
        }

        public class Update
        {
            public static string Success(string _name)
            {
                return "Cập nhật '" + _name + "' thành công";
            }
            public static string Fail(string _name, string _error)
            {
                return "Cập nhật '" + _name + "' thất bại. Lỗi: " + _error;
            }
        }
    }
}
