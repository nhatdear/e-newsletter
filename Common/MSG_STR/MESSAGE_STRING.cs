﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class MESSAGE_TITLE
    {
        public const string INFO = "Thông báo";
        public const string WARNING = "Cảnh báo";
        public const string ERROR = "Lỗi";
    }

  
    public class AUTH_MESSAGE_STRING
    {
        public const string WRONG_USERNAME = "Sai tên đăng nhập.";
        public const string WRONG_PASSWORD = "Sai mật khẩu.";
        public const string DUPLICATE_USERNAME = "Tên đăng nhập đã tồn tại.";
        public const string UNVALID_REGISTER_USER = "Thông tin đăng ký không hợp lệ.";
        public const string UNVALID_LOGIN_USER = "Tên đăng nhập hay mật khẩu không đúng.";
    }
}
