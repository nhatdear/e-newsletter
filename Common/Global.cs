﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Configuration;

namespace Common
{
    public class Global
    {
        public static bool CheckUserPrivacy = true;
        public static string ProjectName = "BlueScope";
        public static string DoNotHavePrivacy = "Bạn chưa được phân quyền cho chức năng này.";
        public static ConnectionInfo ConnectionInfo = new ConnectionInfo(@"(local)",@"QLDT");
        public static MailServerInfo MailServerInfo_ = new MailServerInfo();
        public static String HostDomain = "http://localhost";
        private static int PageSize_ = 10;
        public static int PageSize
        {
            get
            {
                return PageSize_;
            }
            set
            {
                   PageSize_ = value;
            }
        }

        public static string ProductName = "Project Manager";
        public static System.Nullable<System.DateTime> ParseDateTime(string datetime)
        {
            try
            {
                string[] datetimes = datetime.Split("/".ToCharArray());
                if (datetimes == null || datetimes.Length != 3)
                {
                    return null;
                }              


                //String format = "yyyy/MM/dd";
                //DateTime temp = DateTime.ParseExact(datetime, format, CultureInfo.InvariantCulture);

                //System.Nullable<System.DateTime> ret = new DateTime(temp.Year, temp.Month, temp.Day, 12, 0, 0);
                System.Nullable<System.DateTime> ret = new DateTime(int.Parse(datetimes[0]), int.Parse(datetimes[1]), int.Parse(datetimes[2]), 12, 0, 0);
                                
                return ret;
            }
            catch 
            {
                return null;
            }
        }

        public static String DateTimeToString(DateTime datetime)
        {
            try
            {
                //String ret = datetime.ToString("d", CultureInfo.CreateSpecificCulture("zh-TW"));
                String ret = datetime.ToString("yyyy/MM/dd");
                return ret;
            }
            catch
            {
                return null;
            }
        }

        public static String ParseYearMonth(DateTime datetime)
        {
            try
            {
                String ret = datetime.ToString("yyyy/MM");
                return ret;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToFirstTimeInDay(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, input.Value.Day, 0, 0, 0);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToLastTimeInDay(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, input.Value.Day, 23, 59, 59);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToFirst(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, 1,0,0,0);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToLast(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, DateTime.DaysInMonth(input.Value.Year,input.Value.Month),23,59,59);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static String DefaultFromDate()
        {
            DateTime tmpDate = new DateTime(DateTime.Now.Year,1,1);
            return Common.Global.DateTimeToString(tmpDate);
        }

        public static String DefaultToDate()
        {
            DateTime tmpDate = new DateTime(DateTime.Now.Year,12, 31);
            return Common.Global.DateTimeToString(tmpDate);
        }

        public static String encodeBase64(String value)
        {
            try
            {
                if (value == null)
                    value = "";
                return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(value), Base64FormattingOptions.None);
            }
            catch /*(Exception e)*/
            {
                return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(""), Base64FormattingOptions.None);
            }
        }

        public static ConnectionInfo GetConnectionInfo()
        {
            string ServerName = ConfigurationManager.AppSettings["ServerName"].ToString();
            string DatabaseName = ConfigurationManager.AppSettings["DatabaseName"].ToString();
            string UserName = ConfigurationManager.AppSettings["UserName"].ToString();
            string Password = ConfigurationManager.AppSettings["Password"].ToString();
            string Authentication = ConfigurationManager.AppSettings["Authentication"].ToString();
            ConnectionInfo.ServerName = ServerName;
            ConnectionInfo.DatabaseName = DatabaseName;
            ConnectionInfo.UserName = UserName;
            ConnectionInfo.Password = Password;
            ConnectionInfo.Authentication = (Authentication == "") ? Common.ConnectionInfo.WIN_AUTHENTICATION_MODE : Common.ConnectionInfo.SQL_AUTHENTICATION_MODE;
            return ConnectionInfo;
        }

        #region Nghia
        /// <summary> Lưu thông tin nhân viên đăng nhập from</summary>
        public static Common.LoginInfo userLoginInfo;
        public static Common.UserInfo _UserLoginForm;
        #endregion
    }
}
