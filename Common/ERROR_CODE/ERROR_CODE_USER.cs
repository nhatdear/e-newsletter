﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.ComponentModel;
namespace Common.ERROR_CODE
{
    /// <summary>
    /// ERROR_CODE_LOGIN
    /// </summary>
    /// <author>Nguyen Minh Tien</author>
    public enum ERROR_CODE_LOGIN
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Sai mật khẩu.")]
        WRONG_PASS = 2,
        [DescriptionAttribute("Sai tên đăng nhập.")]
        WRONG_USERNAME = 3,
        [DescriptionAttribute("Sai số Imei.")]
        WRONG_IMEI = 4,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1
    }

    public enum ERROR_CODE_CHANGE_PASS_WORD
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Mật khẩu cũ không đúng.")]
        WRONG_PASS = 2,
        [DescriptionAttribute("Sai tên đăng nhập.")]
        WRONG_USERNAME = 3,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1,
    }
}
