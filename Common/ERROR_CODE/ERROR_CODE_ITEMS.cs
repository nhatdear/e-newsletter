﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.ComponentModel;
namespace Common.ERROR_CODE
{
    public enum ERROR_CODE_ITEMS_ADDNEW
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS=1,
        
        [DescriptionAttribute("Lỗi hệ Thống.")]
        SYSTEM_ERROR = -1,
       
        [DescriptionAttribute("Dữ liệu đã tồn tại.")]
        EXIT_DATA = 2,
       
        [DescriptionAttribute("Dữ liệu đã được Update.")]
        UPDATE_DATA = 3,
        DUPPLICATE=0
        
    }
    public enum ERROR_CODE_ITEMS_UPDATE
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Lỗi cập nhật dòng thứ n.")]
        UPDATE_ROW_ERROR = 2,
        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        EXIT_DATA = 3,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1,
        DUPPLICATE = 0
    }
    public enum ERROR_CODE_ITEMS_GET_BY_ID
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        SYSTEM_ERROR = -1,
        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        DO_NOT_EXIST
    }
    public enum ERROR_CODE_ITEMS_CHECK_DUPPLICATE
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        SYSTEM_ERROR = -1
    }
    public enum ERROR_CODE_ITEMS_SELECT
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        SYSTEM_ERROR = -1
    }
}
