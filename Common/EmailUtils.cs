﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Common
{
    public class EmailUtils 
    {
        public static string SendSimpleMailByContentString(MailServerInfo msi, string emailTo, string subject, string contentString, List<String> attachedFilesPath = null) {
            try {
                Chilkat.MailMan mailman = new Chilkat.MailMan();

                //  Any string argument automatically begins the 30-day trial.
                bool success = mailman.UnlockComponent("30-day trial");
                if (success != true) {
                    Console.WriteLine(mailman.LastErrorText);
                }

                //  Set the SMTP server.
                mailman.SmtpHost = msi.MailServer;

                mailman.SmtpUsername = msi.UserName;
                mailman.SmtpPassword = msi.Password;

                mailman.SmtpSsl = true;
                mailman.SmtpPort = 465;

                //  Create a new email object
                Chilkat.Email email = new Chilkat.Email();

                email.Subject = subject;
                email.Body = contentString;
                email.SetHtmlBody(contentString);
                email.From = "Alchemy Asia<no-reply@alchemy-asia.com>";
                success = email.AddTo("",emailTo);

                success = mailman.SendEmail(email);
                if (success != true) {
                    Console.WriteLine(mailman.LastErrorText);
                }

                success = mailman.CloseSmtpConnection();
                if (success != true) {
                    Console.WriteLine("Connection to SMTP server not closed cleanly.");
                }

                Console.WriteLine("Mail Sent!");

                return "SUCCESS";
            } catch (Exception ex) {
                return "ERROR :" + ex.ToString();
            }
        }

        public static string SendSimpleMailByContentString(MailServerInfo msi, string[] emailTo, string subject, string contentString, List<String> attachedFilesPath = null) {
            try {
                Chilkat.MailMan mailman = new Chilkat.MailMan();

                //  Any string argument automatically begins the 30-day trial.
                bool success = mailman.UnlockComponent("30-day trial");
                if (success != true) {
                    Console.WriteLine(mailman.LastErrorText);
                }

                //  Set the SMTP server.
                mailman.SmtpHost = msi.MailServer;

                mailman.SmtpUsername = msi.UserName;
                mailman.SmtpPassword = msi.Password;

                mailman.SmtpSsl = true;
                mailman.SmtpPort = 465;

                //  Create a new email object
                Chilkat.Email email = new Chilkat.Email();

                email.Subject = subject;
                email.Body = contentString;
                email.SetHtmlBody(contentString);
                email.From = "Alchemy Asia<no-reply@alchemy-asia.com>";
                foreach (var mt in emailTo) {
                    email.AddTo("",mt);
                }

                success = mailman.SendEmail(email);
                if (success != true) {
                    return mailman.LastErrorText;
                }

                success = mailman.CloseSmtpConnection();
                if (success != true) {
                    return "Connection to SMTP server not closed cleanly.";
                }
                return "SUCCESS";
            } catch (Exception ex) {
                return "ERROR :" + ex.ToString();
            }
        }

    }
}
