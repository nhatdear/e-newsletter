﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Common
{
    public class XML_Utils
    {

        public static string getValueFromNote(string path, string nodeName, string childName)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(path); // suppose that myXmlString contains "<Names>...</Names>"

            XmlNodeList xnList = xml.SelectNodes(String.Format("/{0}/{1}",nodeName,childName));
            foreach (XmlNode xn in xnList)
            {
                string value = xn.InnerText;
                return value;
            }
            return "";
        }
    }
}
