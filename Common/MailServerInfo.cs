﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class MailServerInfo
    {
        #region Members
        private string _mailServer = "smtp.gmail.com";//"relay-hosting.secureserver.net";//"smtp.gmail.com";
        private int _port = 587;//3535;//25;// 587;
        private string _userName = "saledatabasetest@gmail.com";
        private string _password = "@123456789";
        private bool _EnableSsl = false;
        #endregion

        #region Properties
        public string MailServer
        {
            get { return _mailServer; }
            set { _mailServer = value; }
        }

        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public bool EnableSSL
        {
            get { return _EnableSsl; }
            set { _EnableSsl = value; }
        }
        
        #endregion Properties
    }
}
